#include "ipbus/Server.h"
#include "ipbus/Packet.h"
#include "ipbus/Status.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <netdb.h>
#include <sys/time.h>

using namespace std;
using namespace ipbus;

Server::Server(uint32_t port){

  cout << "Starting server on port " << port << endl;

  //Init UDP socket
  m_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  //Set reuse address
  int reuse_addr=1;
  setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, (const void*)&reuse_addr, sizeof(reuse_addr));
  //Set reuse port
  int reuse_port=1;
  setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEPORT, (const void*)&reuse_port, sizeof(reuse_port));
  //Set timeout 1 second
  struct timeval tv;
  tv.tv_sec = 1;
  tv.tv_usec = 0;
  setsockopt(m_sockfd,SOL_SOCKET,SO_RCVTIMEO,&tv,sizeof(tv));
  //Disable nagle's algorighm
  int nonagle = 1;
  setsockopt(m_sockfd,IPPROTO_TCP, nonagle, (char *)&nonagle, sizeof(nonagle));
  //bind to port
  struct sockaddr_in serv_addr;
  //memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);
  int ret = ::bind(m_sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
  if(ret<0){cout << "ERROR binding to port " << port << endl;}

  m_pb = new PacketBuilder();
  m_trans = new Transaction();
  m_pkid = 0;
  m_verbose = false;
}

Server::~Server(){

  delete m_pb;
  delete m_trans;
  
}

void Server::AddRegister(uint32_t address, std::function<uint32_t(void)> rc, std::function<void(uint32_t val)> wc){

  if(rc){m_rcs[address]=rc;}
  if(wc){m_wcs[address]=wc;}

}

void Server::AddRegister(uint32_t address, std::function<uint32_t(uint32_t pos)> rc, std::function<void(uint32_t pos,uint32_t val)> wc){

  if(rc){m_rfs[address]=rc;}
  if(wc){m_wfs[address]=wc;}
  
}

void Server::AddRegisterRead(uint32_t address, std::function<uint32_t(void)> rc){

  if(rc){m_rcs[address]=rc;}
  
}

void Server::AddRegisterRead(uint32_t address, std::function<uint32_t(uint32_t pos)> rc){

  if(rc){m_rfs[address]=rc;}
  
}

void Server::AddRegisterWrite(uint32_t address, std::function<void(uint32_t val)> wc){

  if(wc){m_wcs[address]=wc;}
  
}

void Server::AddRegisterWrite(uint32_t address, std::function<void(uint32_t pos,uint32_t val)> wc){

  if(wc){m_wfs[address]=wc;}
  
}

void Server::Start(){
  if(m_cont){
    cout << "Server already running" << endl;
    return;
  }
  cout << "Starting server thread" << endl;  
  m_thread = thread(&Server::Run,this);
}

void Server::Stop(){
  cout << "Stopping server thread" << endl;  
  m_cont=false;
  m_thread.join();
}

void Server::Run(){
  
  m_cont=true;
  
  while(m_cont){

    struct sockaddr_in cli_addr;
    socklen_t cli_addr_len = sizeof(cli_addr);
		
    int nb=recvfrom(m_sockfd, m_pb->GetBytes(), 2048, MSG_WAITALL, (struct sockaddr *) &cli_addr, &cli_addr_len); 
    if(nb<=0){continue;}
    
    m_pb->Unpack();
    
    Packet * reply;
    
    if(m_pb->GetHeader()->GetType()==PacketHeader::STATUS){

      cout << "Received status request" << endl;
      cout << " Next PacketId: " << m_pkid << endl;
      
      Status * status = (Status*) m_pb->GetPacket();
      status->SetNextExpectedPacketId(m_pkid);
      reply = status;
      
    }else if(m_pb->GetHeader()->GetType()==PacketHeader::CONTROL && 
             m_pb->GetHeader()->GetPkid()==m_pkid){

      cout << "Received transaction request" << endl;
      cout << " PacketId: " << m_pkid << endl;
      
      Transaction * req = (Transaction*) m_pb->GetPacket();
      
      reply = m_trans;
      m_trans->SetPkid(m_pkid);
      m_trans->GetTransactionHeader()->SetInfo(TransactionHeader::RESPONSE);
      m_trans->GetTransactionHeader()->SetType(req->GetTransactionHeader()->GetType());
      m_trans->GetTransactionHeader()->SetSize(req->GetTransactionHeader()->GetSize());
      m_trans->Clear();
      m_trans->SetSize(req->GetSize());
      m_trans->SetAddress(req->GetAddress());
	  
      if(req->GetTransactionHeader()->GetType()==TransactionHeader::READ){
        cout << " Read request size " << req->GetTransactionHeader()->GetSize() << endl; 
        for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
          if(m_rcs[req->GetAddress()+i]){m_trans->SetData(i,m_rcs[req->GetAddress()+i]());}
        }
      }else if(req->GetTransactionHeader()->GetType()==TransactionHeader::READFIFO){
        cout << " ReadFifo request" << endl;      
        if(m_rcs[req->GetAddress()]){
          for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
            m_trans->SetData(i,m_rcs[req->GetAddress()]());
          }
        }else if(m_rfs[req->GetAddress()]){
          for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
            m_trans->SetData(i,m_rfs[req->GetAddress()](i));
          }
        }
      }else if(req->GetTransactionHeader()->GetType()==TransactionHeader::WRITE){
        cout << " Write request size " << req->GetTransactionHeader()->GetSize() << endl; 
	for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
          if(m_wcs[req->GetAddress()+i]){m_wcs[req->GetAddress()+i](req->GetData(i));}
        }
      }else if(req->GetTransactionHeader()->GetType()==TransactionHeader::WRITEFIFO){
        cout << " WriteFifo request" << endl;
        if(m_wcs[req->GetAddress()]){
	  for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
	    m_wcs[req->GetAddress()](req->GetData(i));
	  }
	}else if(m_wfs[req->GetAddress()]){
	  for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
	    m_wfs[req->GetAddress()](i,req->GetData(i));
	  }
	}
      }else if(req->GetTransactionHeader()->GetType()==TransactionHeader::RMWBITS){
        cout << " RMWB request" << endl;
        for(uint32_t i=0;i<req->GetTransactionHeader()->GetSize();i++){
          if(m_wcs[req->GetAddress()+i]){m_wcs[req->GetAddress()+i](req->GetData(i));}
        }
      }
      
      m_pkid++;
      if(m_pkid>0xFFFF){m_pkid=0;}
      
    }else{
      cout << "Received invalid request " << endl
           << " Expected PacketId " << m_pkid << endl 
           << " Received PacketId " << m_pb->GetHeader()->GetPkid() << endl;
      continue;
    }
    
    reply->Pack();
    
    sendto(m_sockfd, reply->GetBytes(), reply->GetLength(), 0, (const struct sockaddr *) &cli_addr, sizeof(cli_addr)); 
    
  }
}
