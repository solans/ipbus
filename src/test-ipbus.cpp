/**
 * Test the ipbus communication
 * Author: Carlos.Solans@cern.ch
 *
 */
#include <iostream>
#include <cstring>
#include "ipbus/Uhal.h"

using namespace std;
using namespace ipbus;

int main(int argc, const char* argv[]){

  if(argc<3){
    cout << "Usage: " << argv[0] << " [-v] <connstr> <arguments> " << endl
		 << " arguments: " << endl
		 << "\t read [address]" << endl
		 << "\t write [address] [value]" << endl
		 << "\t rmw [address] [value] [startbit] [nbits]" << endl;
    return 0;
  }

  bool verbose = false;

  for(int i=1;i<argc;i++){
    const char * pos=strstr(argv[i],"-v");
    if(pos==NULL) continue;
    verbose=true;
    for(int j=i;j<argc;j++){
      argv[j]=argv[j+1];
    }
    argc--;
  }

  const char * address = argv[1];
  string func(argv[2]);
  uint offset = strtoul(argv[3],NULL,16);

  Uhal::SetVerbose(verbose);
  Uhal * client = new Uhal(address);

  if(verbose) cout << "Synced: " << client->IsSynced() << endl;
  
  if(func=="read"){ 
    uint value = 0;
    client->Read(offset,value);
    cout << "Read from: 0x" << hex << offset << " value: 0x" << value << dec << endl;
  }
  else if(func=="write"){
    if(argc<4){ cout << "Missing arguments" << endl;}
    else{
      uint value = strtoul(argv[4],NULL,16);
      cout << "Write to: 0x" << hex << offset << " value: 0x" << value << dec << endl;
      client->Write(offset, value);
    }
  }
  else if(func=="rmw"){
    if(argc<6){ cout << "Missing arguments" << endl;}
    else{
      uint value = strtoul(argv[4],NULL,10);
      uint start = strtoul(argv[5],NULL,10);
      uint nbits = strtoul(argv[6],NULL,10);
      cout << "RMW to: 0x" << hex << offset << dec
           << " value: "   << value 
           << " start: "   << start 
           << " nbits: "   << nbits 
           << endl;
      client->RMWB(offset, value, start, nbits);
    }
    
  }
  else{
    cout << "Argument " << func << " not understood" << endl;
  }

  delete client;
  
  return 0;
}
