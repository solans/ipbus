#include "ipbus/Transaction.h"
#include <iostream>
#include <unistd.h>
#include <chrono>

using namespace std;
using namespace ipbus;

int main(int argc, const char* argv[]){

  bool verbose = false;
  int unpacks = 100;
  
	for(int i=1;i<argc;i++){
		const char * pos=strstr(argv[i],"-h");
		if(pos==NULL) continue;
 		cout << "Usage: " << argv[0] << " [-v] [-n] " << endl
	       << " -v : enable verbose mode" << endl
         << " -n : number of unpacks" << endl;
    return 0;
  }
  
  for(int i=1;i<argc;i++){
		const char * pos=strstr(argv[i],"-v");
		if(pos==NULL) continue;
		verbose=true;
	}

	for(int i=1;i<argc;i++){
		const char * pos=strstr(argv[i],"-n");
		if(pos==NULL) continue;
		if(strlen(argv[i])>2){
			unpacks = atoi(&argv[i][2]);
		}else{
			unpacks = atoi(argv[i+1]);
		}
	}
  
  Transaction * t = new Transaction();
  
  for(int sz=1000;sz>0;sz--){
  
    t->SetSize(sz);
  
    auto t_start = std::chrono::high_resolution_clock::now();

    for(int i=0; i<unpacks; i++){
      t->Unpack();
    }

    auto t_end = std::chrono::high_resolution_clock::now();

    double et = std::chrono::duration<double, std::micro>(t_end-t_start).count();
  
    cout << "Transaction size:          " << sz << endl;
    cout << "Number of unpacks:         " << unpacks << endl;
    cout << "Elapsed time [us]:         " << et << endl;
    cout << "Time per transaction [us]: " << (et/unpacks) << endl;
  }
  cout << "Clean-up the house" << endl;
  delete t;
  cout << "Have a nice day" << endl;
  return 0;
}