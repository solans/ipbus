#ifndef TransactionHeader_h
#define TransactionHeader_h 1

#include <stdint.h>

namespace ipbus{

  /**
   * Each Transaction contains a 32-bit header with contains the protocol version (4 bits), 
   * the Transaction ID (12 bits), the size of the transaction (8 bits),
   * the Transaction type (4 bits), and the info (4 bits).
   * There can be up to 4095 Transactions in one single Packet.
   * 
   * @brief IPbus 2.0 Transaction header
   * @author Carlos.Solans@cern.ch
   **/
  class TransactionHeader{

  public:
    
    //Type values
    static const uint32_t READ=0;
    static const uint32_t WRITE=1;
    static const uint32_t READFIFO=2;
    static const uint32_t WRITEFIFO=3;
    static const uint32_t RMWBITS=4;
    
    //Info values
    static const uint32_t RESPONSE=0x0;
    static const uint32_t REQUEST=0xF;

  private:
    
    uint32_t pver; //Protocol version 4 bits [28-31]
    uint32_t trid; //Transaction ID 12 bits [16-27]
    uint32_t size; //Number of words 8 bits [8-15]
    uint32_t type; //Type ID 4 bits [4-7]
    uint32_t info; //Info code 4 bits [0-3]
    
    uint32_t head;

  public:

    /**
     * @brief Construct an empty TransactionHeader of type 2.0 and info \c 0xF.
     **/
    TransactionHeader();

    /**
     * @brief Empty destructor
     **/
    ~TransactionHeader();

    /**
     * @brief Get the Transaction size (0 to 15)
     * @return Integer value of the Transaction size.
     **/
    uint32_t GetSize();
    
    /**
     * @brief Get the Transaction type (TransactionHeader::READ,
     * TransactionHeader::WRITE,TransactionHeader::READFIFO, 
     * TransactionHeader::WRITEFIFO, TransactionHeader::RMWBITS)
     * @return Integer value of the Transaction type
     **/
    uint32_t GetType();
    
    /**
     * @brief Get the info
     * @return Integer value of the info. Possible values are
		 * TransactionHeader::REQUEST or TransactionHeader::RESPONSE.
     **/
    uint32_t GetInfo();
    
    /**
     * @brief Get the Transaction ID
     * @return Integer value of the Transaction ID (0 to 4095).
     **/
    uint32_t GetTrid();

    /**
     * @brief Get the protocol version
     * @return Integer value of the protocol version. Should be 2.
     **/
    uint32_t GetPver();

    /**
     * @brief Get the integer value of the Transaction header (32-bit).
     * @return Integer value of the Transaction header.
     **/
    uint32_t GetInt();

    //uint8_t* getBytes();

    /**
     * @brief Set the Transaction size (0 to 15)
     * @param v Integer value of the Transaction size.
     **/
    void SetSize(uint32_t v);

    /**
     * @brief Set the Transaction type (TransactionHeader::READ,
     * TransactionHeader::WRITE,TransactionHeader::READFIFO, 
     * TransactionHeader::WRITEFIFO, TransactionHeader::RMWBITS)
     * @return v Integer value of the Transaction type
     **/
    void SetType(uint32_t v);

    /**
     * @brief Set the info
     * @param v Integer value of the info. Should be 0xF.
     **/
    void SetInfo(uint32_t v);
    
    /**
     * @brief Set the Transaction ID
     * @param v Integer value of the Transaction ID (0 to 4095).
     **/
    void SetTrid(uint32_t v);

    /**
     * @brief Set the protocol version
     * @param v Integer value of the protocol version. Should be 2.
     **/
    void SetPver(uint32_t v);

    /**
     * @brief Set the integer value of the Transaction header (32-bit).
     * @param v The value of the Transaction header.
     **/
    void SetInt(uint32_t v);
    
    /**
     * @brief Read the Transaction header from a byte array b starting at position pos
     * @param b Byte array.
     * @param pos Starting position.
     **/
    void SetBytes(uint8_t* b, uint32_t pos);
    
  private:
    void Pack();
    void Unpack();

  };
}

#endif
