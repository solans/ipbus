#ifndef Status_h
#define Status_h 1

#include "ipbus/Packet.h"
#include <stdint.h>
#include <vector>

namespace ipbus{

  /**
   * The IPbus protocol allows a Status Packet to be sent at any time to 
   * know the status of the device including the next expected Packet ID
   * (Status::GetNextExpectedPacketId).
   * A Status packet does not need a valid Packet ID in the Packet header,
   * therefore it can be used at any time to synchronize the next expected 
   * Packet ID.
   * A Status packet is always of size 16. 
   * One word for the header, and 15 with the content.
   * Word in position 2 contains the next expected packet header, which 
   * contains the next expected packet ID in bits 23 to 8, as in a normal
   * packet header.
   * 
   * 
   * |  31 |  24 |  23 |  16 |  15 |   8 |   7 |   0 |
   * | --- | --- | --- | --- | --- | --- | --- | --- |
   * | 0x20     || 0x0      || 0x0      || 0xF1     || 
   * | MTU in bytes                           |||||||| 
   * | Maximum packet overlap                 ||||||||
   * | Next expected IPbus packet header      |||||||| 
   * | Traff 0  || Traff 1  || Traff 2  || Traff 3  ||
   * | Traff 4  || Traff 5  || Traff 6  || Traff 7  ||
   * | Traff  8 || Traff  9 || Traff 10 || Traff 11 ||
   * | Traff 12 || Traff 13 || Traff 14 || Traff 15 ||
   * | Received IPbus control packet header 0 ||||||||
   * | Received IPbus control packet header 1 ||||||||
   * | Received IPbus control packet header 2 ||||||||
   * | Received IPbus control packet header 3 ||||||||
   * | Sent IPbus control packet header 0     ||||||||
   * | Sent IPbus control packet header 1     ||||||||
   * | Sent IPbus control packet header 2     ||||||||
   * | Sent IPbus control packet header 3     ||||||||
   *     
   *
   * @brief IPbus 2.0 Status Packet
   * @author Carlos.Solans@cern.ch
   **/
  class Status: public Packet{
    
  private:

    std::vector<uint32_t> values; //! Status words

  public:

    //! Number of words in the packet
    static const uint32_t SIZE = 15; 
    
    /**
     * @brief create a new Status packet
     * Create the vector of words with fixed size
     **/
    Status();

    /**
     * @brief Destructor
     **/
    ~Status();

    /**
     * @brief Get the maximum number of packets in memory
     **/
    uint32_t GetBufferSize();

    /**
     * @brief Get the next expected packet ID 
     **/
    uint32_t GetNextExpectedPacketId();

    /**
     * @brief Set the maximum number of packets in memory
     **/
    void SetBufferSize(uint32_t v);

    /**
     * @brief Set the next expected packet ID
     **/
    void SetNextExpectedPacketId(uint32_t v);

    /** 
     * @brief Inform this packet was sent
     * Increment the packet id 
     **/
    void Sent();

    /**
     * @brief Add values to byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    uint32_t AddBytes(uint8_t* bytes, uint32_t pos);

    /**
     * @brief Parse the byte stream
     * @param bytes byte array
     * @param pos a first element of the byte array
     * @return number of bytes processed
     **/
    uint32_t SetBytes(uint8_t* bytes, uint32_t pos);

  };
}
#endif
