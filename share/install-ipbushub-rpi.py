#!/usr/bin/env python

import os
import sys
import time

netcfg=["interface eth0",
        "noipv6",
        "interface eth1",
        "inform 192.168.0.2",
        "noipv6",
       ]

print "Check network settings"
fr = open('/etc/dhcpcd.conf')
fc = []
for line in fr.readlines():
    line=line.strip()
    fc.append(line)
    pass
for newline in netcfg:
    found=False
    for line in fc:
        if newline in line:
            found=True
            break
        pass
    if not found:
        fc.append(newline)
        pass
    pass
fw = open('/tmp/tmp.txt','w')
for line in fc:
    fw.write(line+"\n")
    pass
fw.close()

os.system("cp /etc/dhcpcd.conf /etc/dhcpcd.conf.%s.bak" % time.strftime("%Y%m%d-%H%M%S"))
os.system("mv /tmp/tmp.txt /etc/dhcpcd.conf")

print "Start the service"
os.system("service networking restart")

print "Checkout ipbus"
os.system("git clone https://gitlab.cern.ch/solans/ipbus.git /home/pi/ipbus")

print "Compile ipbus"
os.system("cd /home/pi/ipbus/cmt && ./compile.sh")

print "Install service"
os.system("cp /home/pi/ipbus/share/ipbushub.service /lib/systemd/system/.")

print "Reload daemon"
os.system("systemctl daemon-reload")

print "Start the service"
os.system("service ipbushub start")

print "Have a nice day"

