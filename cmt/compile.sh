#!/bin/bash
#################################
# Standalone compiler for ipbus 
#
# Carlos.Solans@cern.ch
#################################

SRC="../src"
INC="-I ../."
DST="../build"
GCC="c++"
CFL="-Wall -fPIC -std=c++11"
CLK="-shared"
PYI=`python-config --includes`
LIB="-lpthread `pcap-config --libs`"
OBJ=""

tag=`uname`
if [[ "$tag" == "Darwin" ]]; then
    CLK="-shared -dynamiclib -flat_namespace -undefined suppress"
fi

LIB_FILES="ByteTools Packet PacketBuilder PacketHeader Server Status Resend Register Transaction TransactionHeader Uhal"
LIB_NAME="ipbus"
BIN_FILES="test-ipbus ipbushub test-ipbus-server test-ipbus-capture"
MOD_FILE="Herakles"

echo "Removing old files"
rm -rf $DST
mkdir -p $DST
cd ../build

echo "Compiling library sources"
for f in $LIB_FILES
do
  cmd="$GCC $INC $CFL -c -o $DST/$f.o $SRC/$f.cpp"
  echo $cmd
  echo `$cmd`
  OBJ="$OBJ $DST/$f.o"
done

echo "Linking library"
cmd="$GCC $CLK $OBJ -o $DST/lib$LIB_NAME.so"
echo $cmd
echo `$cmd`
LIB="$LIB -L$DST -l$LIB_NAME"

echo "Creating static library"
cmd="ar rcs $DST/lib$LIB_NAME.a $OBJ"
echo $cmd
echo `$cmd`

echo "Compiling binaries"
for f in $BIN_FILES
do
  cmd="$GCC $INC $CFL -c -o $DST/$f.o $SRC/$f.cpp"
  echo $cmd
  echo `$cmd`
  cmd="$GCC $LIB -o $DST/$f $DST/$f.o"
  echo $cmd
  echo `$cmd`
  cmd="$GCC $LIB -o $DST/static-$f $DST/$f.o $OBJ"
  echo $cmd
  echo `$cmd`
done

echo "Compiling python module"
f=$MOD_FILE
cmd="$GCC $INC $CFL $PYI -c -o $DST/$f.o $SRC/$f.cxx"
echo $cmd
echo `$cmd`
cmd="$GCC $CLK $OBJ $DST/$f.o -o $DST/$f.so"
echo $cmd
echo `$cmd`
